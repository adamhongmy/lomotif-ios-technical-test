//
//  BaseButton.swift
//  iOSTechnicalTest
//
//  Created by Adam Hong on 14/09/2019.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable open class BaseButton: UIButton {
    
    /// the corner radius value to have a button with rounded corners.
    @IBInspectable open var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
    
    @IBInspectable open var shadowOpacity: Float = 0 {
        didSet{
            layer.shadowOpacity = shadowOpacity
        }
    }
    
    @IBInspectable open var shadowRadius: CGFloat = 0 {
        didSet{
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable open var shadowOffset: CGSize = CGSize(width: 0, height: 0) {
        didSet{
            layer.shadowOffset = shadowOffset
        }
    }
    
    
    @IBInspectable open var borderColor: UIColor = UIColor.clear {
        didSet{
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable open var borderWidth: CGFloat = 0 {
        didSet{
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable open var roundedCorners: Bool = false {
        didSet {
            self.cornerRadius = self.bounds.height * 0.5
        }
    }
}
