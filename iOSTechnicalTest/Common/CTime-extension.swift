//
//  CTime-extention.swift
//  iOSTechnicalTest
//
//  Created by Adam Hong on 18/09/2019.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import Foundation
import CoreMedia

extension CMTime {
    var stringValue: String {
        let totalSeconds = Int(self.seconds)
        let hours = totalSeconds / 3600
        let minutes = totalSeconds % 3600 / 60
        let seconds = totalSeconds % 3600 % 60
        if hours > 0 {
            return String(format: "%i:%02i:%02i", hours, minutes, seconds)
        } else {
            return String(format: "%02i:%02i", minutes, seconds)
        }
    }
}
