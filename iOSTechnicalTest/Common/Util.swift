//
//  Util.swift
//  iOSTechnicalTest
//
//  Created by Adam Hong on 18/09/2019.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import Foundation

class Util {
    
    class func performBackgroundOperation(using closure: @escaping () -> Void) {
        let dispatchQueue = DispatchQueue.global(qos: .utility)
        dispatchQueue.async {
            closure()
        }
    }
    
    class func performUIUpdate(using closure: @escaping () -> Void) {
        // If we are already on the main thread, execute the closure directly
        if Thread.isMainThread {
            closure()
        } else {
            DispatchQueue.main.async(execute: closure)
        }
    }
    
    
}
