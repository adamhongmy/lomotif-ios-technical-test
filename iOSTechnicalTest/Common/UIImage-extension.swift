//
//  Extension_UIImage.swift
//  iOSTechnicalTest
//
//  Created by Adam Hong on 18/09/2019.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import UIKit

extension UIImage {
    
    static let imageCache = NSCache<AnyObject, AnyObject>()
    
    static func cacheImage(from endPoint: String, completion: @escaping (UIImage?) -> ()) {
        let url = URL(string: endPoint)
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            
            guard error == nil else {
                print(error!.localizedDescription)
                completion(nil)
                return
            }
            
            guard let currData = data else {
                completion(nil)
                return
            }
            
            guard let image = UIImage(data: currData) else {
                completion(nil)
                return
            }
            
            DispatchQueue.main.async {
                imageCache.setObject(image, forKey: endPoint as AnyObject)
            }
            
            completion(image)
            
            }.resume()
    }
}
