//
//  AppNetworking.swift
//  iOSTechnicalTest
//
//  Created by Adam Hong on 14/09/2019.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import Foundation

public enum ConnError: Swift.Error {
    case invalidURL
    case noData
}

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

public struct RequestData {
    public let path: String
    public let method: HTTPMethod
    public let params: [String: String]?
    public let headers: [String: String]?
    
    public init (
        path: String,
        method: HTTPMethod = .get,
        params: [String: String]? = nil,
        headers: [String: String]? = nil
        ) {
        self.path = path
        self.method = method
        self.params = params
        self.headers = headers
    }
}

public protocol RequestType {
    associatedtype ResponseType: Codable
    var data: RequestData { get }
}

public extension RequestType {
    func execute (
        dispatcher: NetworkDispatcher = URLSessionNetworkDispatcher.instance,
        onSuccess: @escaping (ResponseType) -> Void,
        onError: @escaping (Error) -> Void
        ) {
        dispatcher.dispatch(
            request: self.data,
            onSuccess: { (responseData: Data) in
                do {
                    let jsonDecoder = JSONDecoder()
                    let result = try jsonDecoder.decode(ResponseType.self, from: responseData)
                    DispatchQueue.main.async {
                        onSuccess(result)
                    }
                } catch let error {
                    DispatchQueue.main.async {
                        onError(error)
                    }
                }
        },
            onError: { (error: Error) in
                DispatchQueue.main.async {
                    onError(error)
                }
        }
        )
    }
}

public protocol NetworkDispatcher {
    func dispatch(request: RequestData, onSuccess: @escaping (Data) -> Void, onError: @escaping (Error) -> Void)
}

public struct URLSessionNetworkDispatcher: NetworkDispatcher {
    public static let instance = URLSessionNetworkDispatcher()
    private init() {}
    
    public func dispatch(request: RequestData, onSuccess: @escaping (Data) -> Void, onError: @escaping (Error) -> Void) {
        do {
            var urlRequest: URLRequest =  try initUrlRequest(request)
            urlRequest.httpMethod = request.method.rawValue
            
            if let headers = request.headers {
                urlRequest.allHTTPHeaderFields = headers
            }
            
            URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
                if let error = error {
                    onError(error)
                    return
                }
                
                guard let _data = data else {
                    onError(ConnError.noData)
                    return
                }
                
                onSuccess(_data)
                }.resume()
        } catch {
            onError(error)
        }
        
    }
    
    fileprivate func initUrlRequest(_ request: RequestData) throws -> URLRequest  {
        switch request.method {
        case .get:
            do {
                return try getRequestGETMethod(with: request.path, parameters: request.params)
            } catch {
                throw error
            }
        case .post:
            do {
                return try  getRequestPostMethod(with: request.path, parameters: request.params)
            } catch {
                throw error
            }
        }
    }
    
    fileprivate func getRequestGETMethod(with url: String, parameters: [String: String]?) throws -> URLRequest {
        var urlGetMethod = url
        
        if let params = parameters {
            urlGetMethod = url + "?" + params.stringFromHttpParameters()
        }
        
        guard let newUrl = URL(string: urlGetMethod) else {
            throw ConnError.invalidURL
        }
    
        let request = URLRequest(url: newUrl)
        return request
    }
    
    fileprivate func getRequestPostMethod(with url: String, parameters: [String: Any?]?) throws -> URLRequest {
        guard let url = URL(string: url) else {
            throw ConnError.invalidURL
        }
        
        var urlRequest = URLRequest(url: url)
        
        do {
            if let params = parameters {
                urlRequest.httpBody = try JSONSerialization.data(withJSONObject: params, options:[])
            }
        } catch let error {
            throw error
        }
        return urlRequest
    }
}

extension Dictionary where Key : CustomStringConvertible, Value : CustomStringConvertible {
    func stringFromHttpParameters() -> String {
        var parametersString = ""
        for (key, value) in self {
            
            let strValue = String(describing: value)
            parametersString += key.description + "=" + strValue.description  + "&"
        }
        return parametersString
    }
}

