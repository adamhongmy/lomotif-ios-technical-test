//
//  CachedImageView.swift
//  iOSTechnicalTest
//
//  Created by Adam Hong on 18/09/2019.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import UIKit

class CachedImageView: UIImageView {
    
    private var imageEndPoint: String?
    
    private let activityIndicatorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(style: .gray)
        aiv.translatesAutoresizingMaskIntoConstraints = false
        return aiv
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        commonInit()

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        commonInit()
        
    }
    
    
    private func commonInit() {
        self.contentMode = .scaleAspectFill
        self.layer.masksToBounds = true
        layoutActivityIndicator()
    }
    

    
    private func layoutActivityIndicator() {
        activityIndicatorView.removeFromSuperview()

        addSubview(activityIndicatorView)
        NSLayoutConstraint.activate([
            activityIndicatorView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            activityIndicatorView.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            ])

        if self.image == nil {
            activityIndicatorView.startAnimating()
        }
    }
    
    func loadImage(from endPoint: String) {
        
        self.imageEndPoint = endPoint
        
        func setImage(_ image: UIImage) {
            DispatchQueue.main.async {
                self.image = image
                self.activityIndicatorView.stopAnimating()
            }
        }
        
        if let imageFromCache = UIImage.imageCache.object(forKey: endPoint as AnyObject) as? UIImage {
            setImage(imageFromCache)
            return
        }
        
        UIImage.cacheImage(from: endPoint) { (image) in
            guard let imageFromCache = image else {
                return
            }
            setImage(imageFromCache)
        }
    }
}
