//
//  VideoPreviewViewController.swift
//  iOS Technical Test
//
//  Created by Casey Law on 11/12/18.
//  Copyright © 2018 Lomotif. All rights reserved.
//

import UIKit
import AVFoundation
import CoreMedia
import AssetsLibrary
import AVKit

class VideoPreviewViewController: UIViewController {
    
    @IBOutlet weak var vwVideo: UIView!
    @IBOutlet weak var lblVideoDuration: BaseLabel!
    @IBOutlet weak var prgVwVideoDuration: UIProgressView!
    @IBOutlet weak var btnStart: UIButton!
    
    @IBOutlet weak var stckVwLoading: UIStackView!
    @IBOutlet weak var actIndLoading: UIActivityIndicatorView!
    @IBOutlet weak var lblLoadingMessage: UILabel!
    
    let audioPathUrl = "https://audio-ssl.itunes.apple.com/apple-assets-us-std-000001/AudioPreview122/v4/8a/dd/1f/8add1f4d-142c-1317-250d-ff6370962fb8/mzaf_7601694821840779604.plus.aac.p.m4a"
    
    let localVideoURL = URL(fileURLWithPath: Bundle.main.path(forResource: "Movie", ofType: "mp4")!)
    
    var player: AVPlayer! {
        didSet {
            self.player.currentItem?.addObserver(self, forKeyPath: "duration", options: [.new, .initial], context: nil)
            addTimeObserver()
            
            self.playerLayer = AVPlayerLayer(player: player)
            self.playerLayer.frame = self.vwVideo.bounds
            self.playerLayer.videoGravity = .resizeAspect
            
            self.vwVideo.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
            self.vwVideo.layer.addSublayer(playerLayer)
        }
    }
    var playerLayer: AVPlayerLayer!
    var timeObserver: Any?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.player = AVPlayer(url: self.localVideoURL)
        
        prgVwVideoDuration.progress = 0
        
        setInitialState()
    }
    
    @IBAction func onBtnStartPressed(_ sender: BaseButton) {
        setLoadingState()
        fetchAudioFile()
    }
    
    
    func setInitialState() {
        self.lblLoadingMessage.text = ""
        self.actIndLoading.stopAnimating()
        self.btnStart.isHidden = false
        self.stckVwLoading.isHidden = true
    }
    
    func setLoadingState() {
        self.btnStart.isHidden = true
        self.stckVwLoading.isHidden = false
        self.lblLoadingMessage.text = "Downloading Audio File (0%)"
        self.actIndLoading.startAnimating()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.playerLayer.frame = self.vwVideo.bounds
    }
    
    func presentErrorAlertController(error : Error, handler: ((UIAlertAction) -> Void)? = nil) {
        let error = error as NSError
        let message = "\(error.code) :" + error.localizedDescription
        presentAlertController(title: "Error", message:message, handler: handler)
    }
    
    func presentAlertController(message : String, handler: ((UIAlertAction) -> Void)? = nil){
        presentAlertController(title: "Alert", message: message, handler: handler)
    }
    
    func presentAlertController(title: String, message : String, handler: ((UIAlertAction) -> Void)? = nil){
        let alertController = UIAlertController(title:title, message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: handler))
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    deinit {
        print("VideoPreviewViewController deinit")
        self.player.pause()
        self.player.currentItem?.removeObserver(self, forKeyPath: "duration")
        self.player.removeTimeObserver(self.timeObserver!)
    }
}

extension VideoPreviewViewController {
    
    func fetchAudioFile(){
        FetchAudioFile(pathUrl:self.audioPathUrl).execute(onProgress: { [weak self]
            percentage in
            
            let strPercentage = String(format: "%.01f", percentage) + "%"
            self?.lblLoadingMessage.text = "Downloading Audio File (\(strPercentage))"
            
            }, onSuccess: { [weak self] dlAudioUrl in
                
                self?.lblLoadingMessage.text = "Merging..."
                self?.mergeLocalVideoWithDownloadedAudio(audioURL: dlAudioUrl)
                
        }) { [weak self] error in
            
            self?.presentErrorAlertController(error: error)
            self?.setInitialState()
            
        }
    }
    
    func mergeLocalVideoWithDownloadedAudio(audioURL: URL) {
        MergeMediaFiles(videoUrl: self.localVideoURL, with: audioURL).execute(onSuccess: { [weak self] mergedURL in
            
            self?.player = AVPlayer(url: mergedURL)
            self?.player.play()
            
            self?.setInitialState()
            
        }) { [weak self] error in
            
            self?.presentErrorAlertController(error: error)
            self?.setInitialState()
            
        }
    }
    
    
    private func addTimeObserver() {
        let interval = CMTime(seconds: 0.05, preferredTimescale: CMTimeScale(NSEC_PER_SEC))
        let mainQueue = DispatchQueue.main
        self.timeObserver = self.player.addPeriodicTimeObserver(forInterval: interval, queue: mainQueue, using: { [weak self] time in
            guard let currentItem = self?.player.currentItem else {return}
            self?.lblVideoDuration.text = currentItem.currentTime().stringValue
            
            let duration = currentItem.duration
            let currentSeconds = CMTimeGetSeconds(time)
            let totalSeconds = CMTimeGetSeconds(duration)
            let progress: Float = Float(currentSeconds/totalSeconds)
            self?.prgVwVideoDuration.progress = progress
        })
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "duration", let duration = self.player.currentItem?.duration.seconds, duration > 0.0 {
            self.lblVideoDuration.text =  player.currentItem!.duration.stringValue
        }
    }
    
}



