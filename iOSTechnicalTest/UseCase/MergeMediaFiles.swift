//
//  MergeMediaFiles.swift
//  iOSTechnicalTest
//
//  Created by Adam Hong on 19/09/2019.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import Foundation
import AVFoundation
import CoreMedia

struct MergeMediaFiles {
    
    var videoUrl: URL
    var audioUrl: URL
    
    private var onSuccess:((URL)->Void)?
    private var onError:((Error)->Void)?
    
    init(videoUrl: URL, with audioUrl:URL) {
        self.videoUrl = videoUrl
        self.audioUrl = audioUrl
    }
}

extension MergeMediaFiles {
    func execute(onSuccess: @escaping (URL) -> Void,
                 onError: @escaping (Error) -> Void) {
        
        Util.performBackgroundOperation {
            print("Merging")
            
            let mixComposition : AVMutableComposition = AVMutableComposition()
            var mutableCompositionVideoTrack : [AVMutableCompositionTrack] = []
            var mutableCompositionAudioTrack : [AVMutableCompositionTrack] = []
            let totalVideoCompositionInstruction : AVMutableVideoCompositionInstruction = AVMutableVideoCompositionInstruction()
            
            //start merge
            
            let aVideoAsset : AVAsset = AVAsset(url: self.videoUrl)
            let aAudioAsset : AVAsset = AVAsset(url: self.audioUrl)
            
            let semaphore = DispatchSemaphore(value: 0)
            aAudioAsset.loadValuesAsynchronously(forKeys: ["tracks", "playable", "duration"]) {
                var error: NSError?
                let status = aAudioAsset.statusOfValue(forKey: "tracks", error: &error)
                print("aAudioAsset status \(status.rawValue)")
                if status == .loaded {
                    semaphore.signal()
                } else {
                    if(error != nil){
                        print(error!.localizedDescription)
                    }
                    let customError = NSError(domain:"", code:401, userInfo:[ NSLocalizedDescriptionKey: "There is a problem with the video. Please try again."])
                    
                    Util.performUIUpdate { onError(customError) }
                    
                }
            }
            semaphore.wait()
            
            let aVideoAssetTrack : AVAssetTrack = aVideoAsset.tracks(withMediaType: AVMediaType.video)[0]
            let aAudioAssetTrack : AVAssetTrack = aAudioAsset.tracks(withMediaType: AVMediaType.audio)[0]
            
            let compositionVideo = mixComposition.addMutableTrack(withMediaType: AVMediaType.video, preferredTrackID: kCMPersistentTrackID_Invalid)
            let compositionAudio = mixComposition.addMutableTrack(withMediaType: AVMediaType.audio, preferredTrackID: kCMPersistentTrackID_Invalid)
            
            
            mutableCompositionVideoTrack.append(compositionVideo!)
            mutableCompositionAudioTrack.append(compositionAudio!)
            
            do{
                
                try mutableCompositionVideoTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aVideoAssetTrack, at: CMTime.zero)
                
                //audio file is longer then video file; use videoAsset duration instead of audioAsset duration
                try mutableCompositionAudioTrack[0].insertTimeRange(CMTimeRangeMake(start: CMTime.zero, duration: aVideoAssetTrack.timeRange.duration), of: aAudioAssetTrack, at: CMTime.zero)
                //
                //Use this instead above line if your audiofile and video file's playing durations are same
                
                //            try mutableCompositionAudioTrack[0].insertTimeRange(CMTimeRangeMake(kCMTimeZero, aVideoAssetTrack.timeRange.duration), ofTrack: aAudioAssetTrack, atTime: kCMTimeZero)
                
            }catch{
                print("error: \(error.localizedDescription)")
                Util.performUIUpdate { onError(error) }
            }
            
            totalVideoCompositionInstruction.timeRange = CMTimeRangeMake(start: CMTime.zero,duration: aVideoAssetTrack.timeRange.duration )
            
            let mutableVideoComposition : AVMutableVideoComposition = AVMutableVideoComposition()
            mutableVideoComposition.frameDuration = CMTimeMake(value: 1, timescale: 30)
            
            print("Merge done")
            
            let docURL: URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
            
            let savePathUrl = docURL.appendingPathComponent("mergedVideo.mp4")
            
            do {
                let isFileFound: Bool = FileManager.default.fileExists(atPath: savePathUrl.path)
                
                if isFileFound { try FileManager.default.removeItem(atPath:savePathUrl.path) }
            
            } catch {
                print(error.localizedDescription)
            }
            
            let assetExport: AVAssetExportSession = AVAssetExportSession(asset: mixComposition, presetName: AVAssetExportPresetHighestQuality)!
            assetExport.outputFileType = AVFileType.mp4
            assetExport.outputURL = savePathUrl
            assetExport.shouldOptimizeForNetworkUse = true
            
            assetExport.exportAsynchronously { () -> Void in
                switch assetExport.status {
                    
                case .completed:
                    print("success")
                    Util.performUIUpdate { onSuccess(assetExport.outputURL!) }
                    
                case  .failed, .cancelled:
                    print("failed / cancelled \(String(describing: assetExport.error))")
                    Util.performUIUpdate { onError(assetExport.error!) }
                
                default:
                    print("complete")
                }
            }
            
            
        }
    }
}
