//
//  FetchPictureFeedList.swift
//  iOSTechnicalTest
//
//  Created by Adam Hong on 14/09/2019.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import Foundation

struct FetchPictureFeedList: RequestType {
    
    let urlPath: String = "https://pixabay.com/api/"
    let httpMethod: HTTPMethod = .get
    let key: String = "10961674-bf47eb00b05f514cdd08f6e11"
    let query: String = "flower"
    
    let atPage: Int
    
    typealias ResponseType = PictureFeedPage
    
    var data: RequestData {
        return RequestData(path: urlPath, method: httpMethod, params: ["key": key, "q":query, "page": String(atPage)])
    }
}

extension FetchPictureFeedList {
    
    func execute(onSuccess: @escaping ([PictureFeedModel]) -> Void,
                 onError: @escaping (Error) -> Void) {
        self.execute(onSuccess: {(feedPage: PictureFeedPage) in
            onSuccess(feedPage.hits)
        }, onError: onError)
    }
    
}
