//
//  FetchAudioFile.swift
//  iOSTechnicalTest
//
//  Created by Adam Hong on 19/09/2019.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import Foundation

class FetchAudioFile: NSObject {
    var audioPathUrl: String!
    
    lazy private var session : URLSession = {
        let config = URLSessionConfiguration.ephemeral
        config.allowsCellularAccess = false
        let session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
        return session
    }()
    
    private var task : URLSessionTask!
    
    private var onProgress:((Float)->Void)?
    private var onSuccess:((URL)->Void)?
    private var onError:((Error)->Void)?
    
    private var percentageWritten:Float = 0.0
    private var taskTotalBytesWritten = 0
    private var taskTotalBytesExpectedToWrite = 0
    
    init(pathUrl: String) {
        self.audioPathUrl = pathUrl
    }

}

extension FetchAudioFile {
    func execute(onProgress: @escaping (Float) -> Void,
                 onSuccess: @escaping (URL) -> Void,
                 onError: @escaping (Error) -> Void) {
        
        self.onProgress = onProgress
        self.onSuccess = onSuccess
        self.onError = onError
        
        let url = URL(string: self.audioPathUrl)!
        startDownloadAudioFile(url: url)
       
    }
    
    
    private func startDownloadAudioFile(url: URL){
        if self.task != nil { return }
        let req = URLRequest(url:url)
        let task = self.session.downloadTask(with: req)
        self.task = task
        task.resume()
    }
}

extension FetchAudioFile : URLSessionDownloadDelegate {
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
        
        print("downloaded \(100*totalBytesWritten/totalBytesExpectedToWrite)")
        self.taskTotalBytesWritten = Int(totalBytesWritten)
        self.taskTotalBytesExpectedToWrite = Int(totalBytesExpectedToWrite)
        self.percentageWritten = Float(self.taskTotalBytesWritten) / Float(self.taskTotalBytesExpectedToWrite)
        
        Util.performUIUpdate { self.onProgress?(self.percentageWritten*100) }
    }
    
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        print("completed: error: \(error?.localizedDescription ?? "")")
        self.task = nil
        if (error != nil) {
            Util.performUIUpdate { self.onError?(error!) }
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
        
        self.task = nil
        
        let docUrl:URL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        let desURL = docUrl.appendingPathComponent(downloadTask.response!.suggestedFilename!)
        
        do {
            let isFileFound: Bool = FileManager.default.fileExists(atPath: desURL.path)
            
            if isFileFound { try FileManager.default.removeItem(atPath:desURL.path) }
            try FileManager.default.copyItem(at: location, to: desURL)
            
            Util.performUIUpdate { self.onSuccess?(desURL) }
        } catch {
            Util.performUIUpdate { self.onError?(error) }
        }
    }
    
}
