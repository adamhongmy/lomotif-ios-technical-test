//
//  PictureFeedModel.swift
//  iOSTechnicalTest
//
//  Created by Adam Hong on 14/09/2019.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import Foundation
import UIKit

struct PictureFeedPage: Codable {
    let hits: [PictureFeedModel]
}

struct PictureFeedModel: Codable {
    let id:Int
    let previewURL: String
}
