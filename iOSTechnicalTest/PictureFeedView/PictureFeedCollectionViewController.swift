//
//  PictureFeedCollectionViewController.swift
//  iOS Technical Test
//
//  Created by Casey Law on 11/12/18.
//  Copyright © 2018 Lomotif. All rights reserved.
//

import UIKit



class PictureFeedCollectionViewController: UICollectionViewController {
    private let reuseIdentifier = "PictureFeedCell"
    private let footerViewReuseIdentifier = "ActivityIndicatorFooterView"
    
    private var resultList: [PictureFeedModel] = []
    
    private let sectionInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    private let cellSpacing: CGFloat = 3.0
    private let itemsPerRow: CGFloat = 3.0
    
    private var loadingFooterView: ActivityIndicatorFooterView?
    private var isLoadingMore: Bool = false
    
    private var currentPage: Int = 1
    private var pageLimit: Int = 3

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.collectionView.refreshControl = UIRefreshControl()
        
        self.collectionView.refreshControl!.layer.zPosition = -1
        self.collectionView.refreshControl!.addTarget(self, action: #selector(collectionViewlDidRefresh), for: .valueChanged)
        
        self.collectionView.alwaysBounceVertical = true
        
        // Do any additional setup after loading the view.
        fetchInitialPictureList()
    }
    
    func fetchInitialPictureList(){
        self.collectionView.refreshControl!.beginRefreshing()
        self.currentPage = 1
        FetchPictureFeedList(atPage: self.currentPage).execute(onSuccess: {[weak self] (list:[PictureFeedModel]) in
            print(list)
            self?.resultList.removeAll()
            self?.resultList.append(contentsOf: list)
            
            self?.collectionView.reloadData()
            self?.collectionView.refreshControl!.endRefreshing()
            
        }) { [weak self] (error:Error) in
            self?.presentErrorAlertController(error: error as NSError, handler: { (_) in
                self?.collectionView.refreshControl!.endRefreshing()
            })
        }
    }
    
    func fetchSequentialPictureList() {
        self.loadingFooterView?.startAnimate()

        if self.currentPage >= self.pageLimit {
            self.loadingFooterView?.stopAnimate()
            return
        }
        
        self.isLoadingMore = true
        self.currentPage = self.currentPage + 1
        
        FetchPictureFeedList(atPage: self.currentPage).execute(onSuccess: {[weak self] (list:[PictureFeedModel]) in
            print(list)
            self?.resultList.append(contentsOf: list)
            
            self?.isLoadingMore = false
            self?.collectionView.reloadData()
            
        }) { [weak self] (error:Error) in
            self?.presentErrorAlertController(error: error as NSError, handler: { (_) in
                self?.isLoadingMore = false
                self?.collectionView.reloadData()
            })
            
        }
    }
    
    @objc func collectionViewlDidRefresh() {
        fetchInitialPictureList()
    }
    
    func presentErrorAlertController(error : NSError, handler: ((UIAlertAction) -> Void)? = nil){
        let message = "\(error.code) :" + error.localizedDescription
        presentAlertController(title: "Error", message:message)
    }
    
    func presentAlertController(message : String, handler: ((UIAlertAction) -> Void)? = nil){
        presentAlertController(title: "Alert", message: message, handler: handler)
    }
    
    func presentAlertController(title: String, message : String, handler: ((UIAlertAction) -> Void)? = nil){
        let alertController = UIAlertController(title:title, message:
            message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: handler))
        
        self.present(alertController, animated: true, completion: nil)
    }
}


// MARK: UICollectionViewDataSource & UICollectionViewDelegate
extension PictureFeedCollectionViewController {
    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return resultList.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! PictureFeedCell
        
        let model = resultList[indexPath.row]
        let imgUrl = model.previewURL
        cell.displayImage(url: imgUrl)
        
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionView.elementKindSectionFooter {
            let loadingFooterView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath) as! ActivityIndicatorFooterView
            self.loadingFooterView = loadingFooterView
            self.loadingFooterView?.backgroundColor = UIColor.clear
            return loadingFooterView
        } else {
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: footerViewReuseIdentifier, for: indexPath)
            return headerView
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, willDisplaySupplementaryView view: UICollectionReusableView, forElementKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.loadingFooterView?.prepareInitialAnimation()
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didEndDisplayingSupplementaryView view: UICollectionReusableView, forElementOfKind elementKind: String, at indexPath: IndexPath) {
        if elementKind == UICollectionView.elementKindSectionFooter {
            self.loadingFooterView?.stopAnimate()
        }
    }
    
    //compute the scroll value and play witht the threshold to get desired effect
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (self.currentPage >= self.pageLimit) { return }
        let threshold   = 100.0 ;
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        var triggerThreshold  = Float((diffHeight - frameHeight))/Float(threshold);
        triggerThreshold   =  min(triggerThreshold, 0.0)
        let pullRatio  = min(abs(triggerThreshold),1.0);
        self.loadingFooterView?.setTransform(inTransform: CGAffineTransform.identity, scaleFactor: CGFloat(pullRatio))
        if pullRatio >= 1 {
            self.loadingFooterView?.animateFinal()
        }
        print("pullRation:\(pullRatio)")
    }
    
    //compute the offset and call the load method
    override func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y;
        let contentHeight = scrollView.contentSize.height;
        let diffHeight = contentHeight - contentOffset;
        let frameHeight = scrollView.bounds.size.height;
        let pullHeight  = abs(diffHeight - frameHeight);
        print("pullHeight:\(pullHeight)");
        if pullHeight == 34.0 {
            if (self.loadingFooterView?.isAnimatingFinal)! {
                print("load more trigger")
                self.fetchSequentialPictureList()
            }
        }
    }
}

// MARK: - Collection View Flow Layout Delegate
extension PictureFeedCollectionViewController : UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = cellSpacing * (itemsPerRow + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / itemsPerRow
        
        return CGSize(width: widthPerItem, height: widthPerItem)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        if isLoadingMore {
            return CGSize.zero
        }
        return CGSize(width: collectionView.bounds.size.width, height: 55)
    }
}
