//
//  ActivityIndicatorFooterView.swift
//  iOSTechnicalTest
//
//  Created by Adam Hong on 14/09/2019.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import UIKit

class ActivityIndicatorFooterView: UICollectionReusableView {

    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!

    var isAnimatingFinal:Bool = false
    var currentTransform:CGAffineTransform?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.prepareInitialAnimation()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func setTransform(inTransform:CGAffineTransform, scaleFactor:CGFloat) {
        if isAnimatingFinal {
            return
        }
        self.currentTransform = inTransform
        self.loadingIndicator?.transform = CGAffineTransform.init(scaleX: scaleFactor, y: scaleFactor)
    }
    
    //reset the animation
    func prepareInitialAnimation() {
        self.isAnimatingFinal = false
        self.loadingIndicator?.stopAnimating()
        self.loadingIndicator?.transform = CGAffineTransform.init(scaleX: 0.0, y: 0.0)
    }
    
    func startAnimate() {
        self.isAnimatingFinal = true
        self.loadingIndicator?.startAnimating()
    }
    
    func stopAnimate() {
        self.isAnimatingFinal = false
        self.loadingIndicator?.stopAnimating()
    }
    
    //final animation to display loading
    func animateFinal() {
        if isAnimatingFinal {
            return
        }
        self.isAnimatingFinal = true
        UIView.animate(withDuration: 0.2) {
            self.loadingIndicator?.transform = CGAffineTransform.identity
        }
    }
    
}
