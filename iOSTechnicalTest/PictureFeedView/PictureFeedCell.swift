//
//  PictureFeedCell.swift
//  iOSTechnicalTest
//
//  Created by Adam Hong on 14/09/2019.
//  Copyright © 2019 Lomotif. All rights reserved.
//

import UIKit

class PictureFeedCell: UICollectionViewCell {
    @IBOutlet weak var imageView: CachedImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func displayImage(url: String) {
        imageView.loadImage(from: url)
    }
    
}
